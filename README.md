[![k6](https://img.shields.io/badge/k6-7D64FF.svg?style=for-the-badge&logo=k6&logoColor=white)](https://github.com/grafana/k6)![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E.svg?style=for-the-badge&logo=JavaScript&logoColor=black)

# K6 SAMPLE FRAMEWORK

## Initialization
Read through following guide to how to install k6 on your machine:

https://k6.io/docs/get-started/installation/

## How to run
1. Clone the repo
2. Add Following Config to your Config File located in config/ directory
```
"CREDENTIALS": {
        "email": "some_user@example.com",
        "password": "some_password"
    }
```
3. Run using below commands

```
k6 run --vus 10 --iterations 20 --out json=result/result.json code/scripts/e2e.js

k6 run --vus 10 --iterations 20 --out csv=result/result.csv code/scripts/e2e.js
```
Skipping setup/teardown execution:
``` k6 run --no-setup --no-teardown ... ```
Run locally with config file 
``` k6 run --no-setup -u 10 -i 10 -e configFile=qa_env.json code/scripts/e2e.js ```

where,
``` 
vus - virtual users,

iterations - number of times to run the script, shared among VUs (ex. --vus 10 --iterations 30, each vue executes 3 iterations)

duration - specifies how long the test executes for.
it will run n iterations of the script with specified VUs during the specified time (ex 10s, 10m, 2h etc)

json/CSV - report output file type

-o/--out - output method (ex. cloud)

``` 

Note: All the scripts use shared-iterations executor by default (https://k6.io/docs/using-k6/scenarios/executors/shared-iterations/)
1) --vus 10 --iterations 10 will access 10 times
2) --vus 10 --iterations 20 will access 20 times

TODO:
#1 Add a debugMode flag for logging
#2 Add to actions input checkbox option to save data to dashboard
#3 requirements.txt like implementation for managing dependencies
#4 pass options.stages from environment file