import init from '../common/init.js';
import http from 'k6/http';
import { login } from '../common/client.js';

//HTML Report
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";

//get config details
const config = init.getActiveConfig().CONTENT
const baseURL = config.baseURL;


const userInfo = config.CREDENTIALS

// ** Init block starts ** //
// k6 options is part of init block
export let options = {
    stages: [
        { duration: '1m', target: 10 },
        { duration: '1m', target: 20 },
        { duration: '1m', target: 0 }
    ]
}
// ** End of init block ** //
//setup
export function setup(){
    const response = http.get(`${baseUrl}/get`);
    return { data : response.json() };
}

export default function testSuite() {
    login(userInfo.email, userInfo.password)
    let data = { name: 'Bert' };
    
    let response = http.post(`${baseUrl}/post`, JSON.stringify(data), {
        headers: { 'Content-Type': 'application/json' },
    });

    console.log(response.json().json.name);
}

export function teardown(){

}
export function handleSummary(data) {
	return {
	  "reports/report.html": htmlReport(data),
	  stdout: textSummary(data, { indent: " ", enableColors: true }),
	};
  }