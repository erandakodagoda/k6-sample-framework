export function getRandomString() {

    let rs = "x".repeat(5)
        .replace(/./g, c => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"[Math.floor(Math.random() * 62)]);

    return rs
}

export function getARandomNumber() {
    //get random number from a range [1-3]
    let randomNumber = [Math.ceil(Math.random() * 3)]
    return randomNumber
}